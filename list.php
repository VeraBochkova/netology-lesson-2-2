<?php
$test_files = glob('Tests/*.json');
if (!empty($test_files)):
    foreach ($test_files as $file): ?>
        <form action="test.php" method="GET">
            <fieldset>
            <legend><?php echo basename($file)?></legend>
            <div><input type="hidden" name="test_number" value="<?php echo array_search($file, $test_files) + 1;?>"></div>
            <div><input type="submit" name="get_testing" value="Пройти тестирование"></div>
            </fieldset>
        </form>
    <?php
    endforeach;
endif;
?>
