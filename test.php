<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<div><a href="list.php"><button>Вернуться к выбору тестов</button></a></div>
<br>
<div><a href="admin.php"><button>Вернуться к созданию и загрузке тестов</button></a></div>
<br>

<?php
if (!empty($_GET) && isset($_GET)):
    $number = $_GET["test_number"];
    $file = json_decode(file_get_contents('test' . $number . '.json'), true); ?>
    <form action="test.php" method="POST">
    <?php foreach ($file as $question): ?>
        <fieldset>
            <legend><?php echo $question["quest"]; ?></legend>
            <?php foreach ($question["answer"] as $answer ): ?>
                <label><input name="<?php echo $question["number"]; ?>" type="radio" value="<?php echo $answer; ?>"><?php echo $answer; ?></label>
            <?php endforeach;?>
        </fieldset>
        <br>
    <?php endforeach;?>
        <input type="hidden" name="test_number" value="<?php echo 'test' . $number . '.json';?>">
        <input type="submit" name="get result" value="Показать результат">
    </form>
<?php endif; ?>

<?php
if (!empty($_POST) && isset($_POST)):
    #echo "<pre>";
    #var_dump($_POST);
    $file_name = $_POST["test_number"];
    $file = json_decode(file_get_contents($file_name), true);
    #echo "<pre>";
    #var_dump($file);
    $i = 1;
    foreach ($file as $question):
        if ($_POST[$i] == $question["correct"]): ?>
            <h4><?php echo ("Верный ответ на вопрос" . " " . $i . " " . "!"); ?></h4>
        <?php else: ?> <h4><?php echo ("Неверный ответ на вопрос" . " " . $i . " " . "!"); ?></h4>
        <?php endif;
        $i++;
    endforeach;
endif; ?>

</body>
</html>
